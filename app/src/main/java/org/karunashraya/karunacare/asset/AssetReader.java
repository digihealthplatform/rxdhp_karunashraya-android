package org.karunashraya.karunacare.asset;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.karunashraya.karunacare.app.LApplication;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class AssetReader {

    private static JSONObject jsonObject;

    private static Gson gson;

    public static void init() {
        try {
            InputStream inputStream = LApplication.getAppContext().getAssets().open("input.json");
            byte[]      fileBytes   = new byte[inputStream.available()];
            inputStream.read(fileBytes);
            inputStream.close();
            jsonObject = new JSONObject(new String(fileBytes));
            gson = new Gson();

        } catch (IOException | JSONException e) {
        }
    }

    public static String[] getSpinnerArray(String key) {
        String[] array = new String[1];

        try {
            JSONArray jsonArray = jsonObject.getJSONArray(key);

            array = new String[jsonArray.length() + 1];
            array[0] = "Select";

            for (int i = 0; i < jsonArray.length(); i++) {
                array[i + 1] = jsonArray.getString(i);
            }

        } catch (Exception e) {
        }

        return array;
    }

    public static ItemSpecialization[] getSpecialization() {
        try {
            JSONArray jsonArray = jsonObject.getJSONArray("specialization");
            return gson.fromJson(jsonArray.toString(), ItemSpecialization[].class);

        } catch (Exception e) {
        }

        return null;
    }
}