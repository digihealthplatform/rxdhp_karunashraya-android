package org.karunashraya.karunacare.asset;

import com.google.gson.annotations.SerializedName;

public class ItemUser {

    @SerializedName("emailId")
    private String email;

    @SerializedName("phone")
    private String phone;

    @SerializedName("specialization")
    private String specialization;

    @SerializedName("subSpecialization")
    private String subSpecialization;

    @SerializedName("yearsOfExperience")
    private String yearsOfExperience;

    @SerializedName("yearsOfExperienceInPalliativeCare")
    private String yearsOfExperienceInPalliativeCare;

    @SerializedName("location")
    private String location;

    @SerializedName("state")
    private String state;

    @SerializedName("locationOther")
    private String locationOther;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getSubSpecialization() {
        return subSpecialization;
    }

    public void setSubSpecialization(String subSpecialization) {
        this.subSpecialization = subSpecialization;
    }

    public String getYearsOfExperience() {
        return yearsOfExperience;
    }

    public void setYearsOfExperience(String yearsOfExperience) {
        this.yearsOfExperience = yearsOfExperience;
    }

    public String getYearsOfExperienceInPalliativeCare() {
        return yearsOfExperienceInPalliativeCare;
    }

    public void setYearsOfExperienceInPalliativeCare(String yearsOfExperienceInPalliativeCare) {
        this.yearsOfExperienceInPalliativeCare = yearsOfExperienceInPalliativeCare;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getLocationOther() {
        return locationOther;
    }

    public void setLocationOther(String locationOther) {
        this.locationOther = locationOther;
    }
}
