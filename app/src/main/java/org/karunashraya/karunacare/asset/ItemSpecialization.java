package org.karunashraya.karunacare.asset;

import com.google.gson.annotations.SerializedName;

public class ItemSpecialization {

    @SerializedName("specialization")
    private String specialization;

    @SerializedName("sub_specialization")
    private String[] subSpecialization;

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String[] getSubSpecialization() {
        return subSpecialization;
    }

    public void setSubSpecialization(String[] subSpecialization) {
        this.subSpecialization = subSpecialization;
    }
}