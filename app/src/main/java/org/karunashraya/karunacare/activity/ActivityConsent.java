package org.karunashraya.karunacare.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import org.karunashrayacare.R;

public class ActivityConsent extends AppCompatActivity {

    private static final String TAG = ActivityConsent.class.getSimpleName();
    RecyclerView mRecyclerView;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consent);

        mToolbar = (Toolbar) findViewById(R.id.app_bar_main);
        mToolbar.setTitle(getString(R.string.app_name));

        setSupportActionBar(mToolbar);

        findViewById(R.id.ll_continue).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ActivityConsent.this, ActivityMain.class));
                finish();
            }
        });
    }
}
