package org.karunashraya.karunacare.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.LinearLayout;

import org.karunashraya.karunacare.adapter.AdapterActions;
import org.karunashraya.karunacare.adapter.AdapterOptions;
import org.karunashraya.karunacare.analytics.LAnalytics;
import org.karunashraya.karunacare.app.LApplication;
import org.karunashraya.karunacare.interfaces.ActionCallbacks;
import org.karunashraya.karunacare.interfaces.OptionCallbacks;
import org.karunashrayacare.R;

import static org.karunashraya.karunacare.utils.LConstants.KEY_FILE_NAME;

public class ActivityActions extends AppCompatActivity implements ActionCallbacks, OptionCallbacks {

    private Toolbar mToolbar;

    private AdapterActions mAdapterActions;
    private AdapterOptions mAdapterOptions;

    private RecyclerView mRvActions;
    private RecyclerView mRvOptions;
    private String       mFileName;

    private LAnalytics analytics;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_actions);

        mToolbar = (Toolbar) findViewById(R.id.app_bar_main);
        mToolbar.setTitle(getString(R.string.app_name));

        setSupportActionBar(mToolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (null == savedInstanceState) {
            mFileName = getIntent().getStringExtra(KEY_FILE_NAME);
        } else {
            mFileName = savedInstanceState.getString(KEY_FILE_NAME);
        }

        // Save jsonObject

        mRvActions = (RecyclerView) findViewById(R.id.rv_actions);
        mRvOptions = (RecyclerView) findViewById(R.id.rv_options);

        mRvActions.setLayoutManager(new LinearLayoutManager(this));

        LinearLayoutManager ll = new LinearLayoutManager(this);
        ll.setStackFromEnd(true);
        mRvOptions.setLayoutManager(ll);

        mAdapterOptions = new AdapterOptions(this, this);
        mRvOptions.setAdapter(mAdapterOptions);

        mAdapterActions = new AdapterActions(this, this, mFileName);
        mRvActions.setAdapter(mAdapterActions);
        mAdapterActions.showActions();

        analytics = new LAnalytics(this);
        analytics.setScreenName(mAdapterActions.getTitle());
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(KEY_FILE_NAME, mFileName);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mRvOptions.scrollToPosition(0);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home: {
                finish();
            }
            return false;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onOptionsSet(String[] options) {
        mAdapterOptions.setOptions(options);

        if (2 < mAdapterActions.getItemCount()) {
            mRvActions.smoothScrollToPosition(mAdapterActions.getItemCount() - 1);
        }

        mRvOptions.smoothScrollToPosition(0);
    }

    @Override
    public void onItemExpanded(int position) {
        if (2 < position) {
            mRvActions.smoothScrollToPosition(position);
        }
    }

    @Override
    public void onBackPressed() {
        if (mAdapterActions.onBackPressed()) {
            super.onBackPressed();
        }
    }

    @Override
    public void onOptionSelected(int position) {
        mAdapterActions.optionSelected(position);
    }

    @Override
    public void onOptionsCount(final int count) {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                float weight = 0.0f;

                if (1 == count) {
                    weight = 1.1f;
                } else if (2 == count) {
                    weight = 1.9f;
                } else if (3 == count) {
                    weight = 2.6f;
                } else if (4 == count) {
                    weight = 2.6f;
                } else if (5 <= count) {
                    weight = 3.3f;
                }

                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mRvOptions.getLayoutParams();
//        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.MATCH_PARENT,
//                0,
//                weight);
                params.weight = weight;

                mRvOptions.setLayoutParams(params);
                mRvOptions.scrollToPosition(0);
            }
        }, 120);
    }
}
