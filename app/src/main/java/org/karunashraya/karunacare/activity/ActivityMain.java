package org.karunashraya.karunacare.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.karunashraya.karunacare.adapter.AdapterSymptoms;
import org.karunashraya.karunacare.analytics.LAnalytics;
import org.karunashraya.karunacare.app.LApplication;
import org.karunashraya.karunacare.utils.JsonSyncTask;
import org.karunashraya.karunacare.utils.LPref;
import org.karunashraya.karunacare.utils.LUtils;
import org.karunashrayacare.BuildConfig;
import org.karunashrayacare.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import static org.karunashraya.karunacare.utils.LUtils.generateKey;

public class ActivityMain extends AppCompatActivity {

    private static final String TAG = ActivityMain.class.getSimpleName();
    RecyclerView mRecyclerView;
    private Toolbar mToolbar;

    private LAnalytics analytics;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mToolbar = (Toolbar) findViewById(R.id.app_bar_main);
        mToolbar.setTitle(getString(R.string.app_name));

        setSupportActionBar(mToolbar);

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_actions);

        final GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
        mRecyclerView.setLayoutManager(gridLayoutManager);

        final AdapterSymptoms adapter = new AdapterSymptoms(this, getString(R.string.model_file_name));

        mRecyclerView.setAdapter(adapter);

//        encryptAssets();

        analytics = new LAnalytics(this);
        analytics.setScreenName("Home");
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!LPref.getBooleanPref("data_synced", false)) {
            new JsonSyncTask().execute();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_help: {
                startActivity(new Intent(this, ActivityHelp.class));
            }
            break;

            case R.id.action_about: {
                startActivity(new Intent(this, ActivityAbout.class));
            }
            break;

            case R.id.action_send_feedback: {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", getString(R.string.feedback_mail_id), null));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT,
                        getString(R.string.app_name)
                                + " (v"
                                + BuildConfig.VERSION_NAME
                                + ") - "
                                + getString(R.string.feedback_mail_subject));
                startActivity(Intent.createChooser(emailIntent, ""));
            }
            break;

            case R.id.action_rate_the_app: {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + BuildConfig.APPLICATION_ID)));
                } catch (android.content.ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID)));
                }
            }
            break;
        }

        return super.onOptionsItemSelected(item);
    }
}
