package org.karunashraya.karunacare.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import org.karunashraya.karunacare.utils.LPref;
import org.karunashrayacare.R;

public class ActivitySplashScreen extends AppCompatActivity {

    private boolean backPressed;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!backPressed) {

                    if (!LPref.getBooleanPref("signed_up", false)) {
                        startActivity(new Intent(ActivitySplashScreen.this, ActivitySignUp.class));
                    } else {
                        startActivity(new Intent(ActivitySplashScreen.this, ActivityMain.class));
                    }

                    finish();
                }
            }
        }, 3000);

//        LUtils.encryptAssets();
    }

    @Override
    public void onBackPressed() {
        backPressed = true;
        super.onBackPressed();
    }
}