package org.karunashraya.karunacare.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;

import org.karunashraya.karunacare.analytics.LAnalytics;
import org.karunashrayacare.R;

public class ActivityHelp extends AppCompatActivity {

    private static final String TAG = ActivityHelp.class.getSimpleName();
    RecyclerView mRecyclerView;
    private Toolbar mToolbar;
    private InputMethodManager mInputMethodManager;

    private LAnalytics analytics;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        mToolbar = (Toolbar) findViewById(R.id.app_bar_main);
        mToolbar.setTitle(getString(R.string.app_name) + ": Help");

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        analytics = new LAnalytics(this);
        analytics.setScreenName("Help");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
            }
            break;
        }

        return super.onOptionsItemSelected(item);
    }
}
