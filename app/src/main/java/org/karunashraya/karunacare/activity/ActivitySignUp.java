package org.karunashraya.karunacare.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.karunashraya.karunacare.asset.AssetReader;
import org.karunashraya.karunacare.asset.ItemSpecialization;
import org.karunashraya.karunacare.utils.LPref;
import org.karunashraya.karunacare.utils.LUtils;
import org.karunashrayacare.R;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.VISIBLE;

public class ActivitySignUp extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private static final String TAG = ActivitySignUp.class.getSimpleName();

    private static final int CODE_WRITE_PERMISSION = 100;

    private Toolbar mToolbar;

    @BindView(R.id.et_email)
    EditText etEmail;

    @BindView(R.id.et_phone)
    EditText etPhone;

    @BindView(R.id.sp_specialization)
    Spinner spSpecialization;

    @BindView(R.id.sp_sub_specialization)
    Spinner spSubSpecialization;

    @BindView(R.id.sp_years)
    Spinner spYears;

    @BindView(R.id.sp_years_palliative)
    Spinner spYearsPalliativeCare;

    @BindView(R.id.rg_location)
    RadioGroup rgLocation;

    @BindView(R.id.sp_states)
    Spinner spStates;

    @BindView(R.id.ll_sub_specialization)
    LinearLayout llSubSpecialization;

    @BindView(R.id.tv_other_specialization)
    TextView tvOtherSpecialization;

    @BindView(R.id.et_specialization_other)
    EditText etOtherSpecialization;

    @BindView(R.id.tv_other_sub_specialization)
    TextView tvOtherSubSpecialization;

    @BindView(R.id.et_sub_specialization_other)
    EditText etOtherSubSpecialization;

    @BindView(R.id.tv_other_location)
    TextView tvOtherLocation;

    @BindView(R.id.et_location_other)
    EditText etOtherLocation;

    @BindView(R.id.cb_accept)
    CheckBox cbAccept;

    private ItemSpecialization[] itemSpecializations;

    private String[] specializations;
    private String[] subSpecializations;

    private String[] states;

    // -------------------
    private String phone                             = "";
    private String email                             = "";
    private String specialization                    = "";
    private String subSpecialization                 = "";
    private String yearsOfExperience                 = "";
    private String yearsOfExperienceInPalliativeCare = "";
    private String location                          = "";
    private String state                             = "";
    private String locationOther                     = "";

    private String[] years;
    private String[] yearsInPalliativeCare;

    private InputMethodManager inputMethodManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_singup);
        ButterKnife.bind(this);

        AssetReader.init();

        inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        mToolbar = (Toolbar) findViewById(R.id.app_bar_main);
        mToolbar.setTitle(getString(R.string.app_name));

        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("PalliKare - Sign up");

        itemSpecializations = AssetReader.getSpecialization();

        years = AssetReader.getSpinnerArray("years");
        yearsInPalliativeCare = AssetReader.getSpinnerArray("years_palliative_care");

        specializations = new String[itemSpecializations.length + 1];
        specializations[0] = "Select";

        for (int i = 0; i < itemSpecializations.length; i++) {
            specializations[i + 1] = itemSpecializations[i].getSpecialization();
        }

        states = AssetReader.getSpinnerArray("states");

        LUtils.setSpinnerAdapter(this, spSpecialization, specializations);
        LUtils.setSpinnerAdapter(this, spYears, years);
        LUtils.setSpinnerAdapter(this, spYearsPalliativeCare, yearsInPalliativeCare);
        LUtils.setSpinnerAdapter(this, spStates, states);

        spSpecialization.setOnItemSelectedListener(this);
        spSubSpecialization.setOnItemSelectedListener(this);
        spYears.setOnItemSelectedListener(this);
        spYearsPalliativeCare.setOnItemSelectedListener(this);
        spStates.setOnItemSelectedListener(this);

        rgLocation.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (-1 == indexId) {
                    return;
                }
                location = ((RadioButton) findViewById(indexId)).getText().toString();
            }
        });

        findViewById(R.id.ll_continue).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signUpWithPermissionCheck();
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        if (position <= 0) {
            return;
        }

        inputMethodManager.hideSoftInputFromWindow(etEmail.getWindowToken(), 0);

        switch (adapterView.getId()) {
            case R.id.sp_specialization: {
                specialization = specializations[position];

                if (specialization.equals("Other")) {
                    subSpecializations = null;

                    tvOtherSpecialization.setVisibility(VISIBLE);
                    etOtherSpecialization.setVisibility(VISIBLE);
                    llSubSpecialization.setVisibility(View.GONE);

                } else {
                    tvOtherSpecialization.setVisibility(View.GONE);
                    etOtherSpecialization.setVisibility(View.GONE);
                    etOtherSpecialization.setText("");

                    subSpecializations = getSub(specializations[position]);

                    if (null != subSpecializations) {
                        llSubSpecialization.setVisibility(VISIBLE);
                        LUtils.setSpinnerAdapterNoSelect(this, spSubSpecialization, subSpecializations);
                        subSpecialization = subSpecializations[0];

                    } else {
                        subSpecialization = "";
                        llSubSpecialization.setVisibility(View.GONE);
                    }
                }
            }
            break;

            case R.id.sp_sub_specialization: {

                if (subSpecializations[position].equals("Other")) {
                    subSpecialization = "";
                    tvOtherSubSpecialization.setVisibility(VISIBLE);
                    etOtherSubSpecialization.setVisibility(VISIBLE);

                } else {
                    subSpecialization = subSpecializations[position];
                    tvOtherSubSpecialization.setVisibility(View.GONE);
                    etOtherSubSpecialization.setVisibility(View.GONE);
                    etOtherSubSpecialization.setText("");
                }
            }
            break;

            case R.id.sp_years: {
                yearsOfExperience = years[position];
            }
            break;

            case R.id.sp_years_palliative: {
                yearsOfExperienceInPalliativeCare = yearsInPalliativeCare[position];
            }
            break;

            case R.id.sp_states: {
                state = states[position];

                if (state.equals("Other")) {
                    tvOtherLocation.setVisibility(VISIBLE);
                    etOtherLocation.setVisibility(VISIBLE);

                } else {
                    tvOtherLocation.setVisibility(View.GONE);
                    etOtherLocation.setVisibility(View.GONE);
                    etOtherLocation.setText("");
                    locationOther = "";
                }
            }
            break;
        }
    }

    private String[] getSub(String specialization) {
        for (ItemSpecialization itemSpecialization : itemSpecializations) {
            if (itemSpecialization.getSpecialization().equals(specialization)) {
                return itemSpecialization.getSubSpecialization();
            }
        }
        return null;
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.message_confirm_exit));
        builder.setPositiveButton(getString(R.string.btn_exit), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivitySignUp.super.onBackPressed();
            }
        });
        builder.setNegativeButton(getString(R.string.btn_cancel), null);
        builder.show();
    }

    public void signUpWithPermissionCheck() {

        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (PackageManager.PERMISSION_GRANTED != permissionCheck) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    CODE_WRITE_PERMISSION);
        } else {
            signUp();
        }
    }

    private void signUp() {
        readValues();
        if (isAllFieldsEntered()) {

            showToast("Sign up complete");

            saveValues();
            LPref.putBooleanPref("signed_up", true);

            startActivity(new Intent(ActivitySignUp.this, ActivityMain.class));
            finish();
        }
    }

    private void readValues() {
        email = etEmail.getText().toString();
        phone = etPhone.getText().toString();

        if (VISIBLE == etOtherSpecialization.getVisibility()) {
            specialization = etOtherSpecialization.getText().toString();
        }

        if (VISIBLE == etOtherSubSpecialization.getVisibility()) {
            subSpecialization = etOtherSubSpecialization.getText().toString();
        }

        if (VISIBLE == etOtherLocation.getVisibility()) {
            locationOther = etOtherLocation.getText().toString();
        }
    }

    private boolean isAllFieldsEntered() {

        if (email.equals("") || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            showToast("Enter valid Email ID");
            return false;

        } else if (phone.equals("") || !Patterns.PHONE.matcher(phone).matches()) {
            showToast("Enter valid Phone number");
            return false;

        } else if (specialization.equals("")) {
            showToast("Enter specialization");
            return false;

        } else if (VISIBLE == llSubSpecialization.getVisibility() && subSpecialization.equals("")) {
            showToast("Enter specialization details");
            return false;

        } else if (yearsOfExperience.equals("")) {
            showToast("Select years of experience");
            return false;

        } else if (yearsOfExperienceInPalliativeCare.equals("")) {
            showToast("Select years of experience in palliative care");
            return false;

        } else if (location.equals("")) {
            showToast("Select location");
            return false;

        } else if (state.equals("")) {
            showToast("Select state");
            return false;

        } else if (state.equals("Other") && locationOther.equals("")) {
            showToast("Enter location details");
            return false;

        } else if (!cbAccept.isChecked()) {
            showToast("Please accept the terms and conditions to continue");
            return false;
        }

        return true;
    }

    private void saveValues() {
        LPref.putStringPref("email", email);
        LPref.putStringPref("phone", phone);
        LPref.putStringPref("specialization", specialization);
        LPref.putStringPref("sub_specialization", subSpecialization);
        LPref.putStringPref("years_of_experience", yearsOfExperience);
        LPref.putStringPref("years_of_experience_in_palliative_care", yearsOfExperienceInPalliativeCare);
        LPref.putStringPref("location", location);
        LPref.putStringPref("state", state);
        LPref.putStringPref("locationOther", locationOther);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {
            case CODE_WRITE_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    signUp();
                } else {
                    showToast(getString(R.string.message_permission_denied));
                }
            }
            break;
        }
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}