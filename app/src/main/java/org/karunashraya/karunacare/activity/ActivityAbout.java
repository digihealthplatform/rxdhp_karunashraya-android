package org.karunashraya.karunacare.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import org.karunashraya.karunacare.adapter.AdapterSymptoms;
import org.karunashraya.karunacare.analytics.LAnalytics;
import org.karunashraya.karunacare.app.LApplication;
import org.karunashrayacare.BuildConfig;
import org.karunashrayacare.R;

public class ActivityAbout extends AppCompatActivity {

    private static final String TAG = ActivityAbout.class.getSimpleName();
    RecyclerView mRecyclerView;
    private Toolbar            mToolbar;
    private InputMethodManager mInputMethodManager;

    private LAnalytics analytics;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        mToolbar = (Toolbar) findViewById(R.id.app_bar_main);
        mToolbar.setTitle(getString(R.string.app_name) + ": About");

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        TextView tvVersionName = (TextView) findViewById(R.id.tv_version_name);
        tvVersionName.setText("Version: " + BuildConfig.VERSION_NAME);

        analytics = new LAnalytics(this);
        analytics.setScreenName("About");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
            }
            break;
        }

        return super.onOptionsItemSelected(item);
    }
}
