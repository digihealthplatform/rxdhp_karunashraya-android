package org.karunashraya.karunacare.adapter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.karunashraya.karunacare.app.LApplication;
import org.karunashraya.karunacare.interfaces.ActionCallbacks;
import org.karunashraya.karunacare.items.ItemAction;
import org.karunashraya.karunacare.utils.LUtils;
import org.karunashrayacare.R;

import java.util.ArrayList;

import static org.karunashraya.karunacare.utils.LConstants.KEY_DECISION;
import static org.karunashraya.karunacare.utils.LConstants.KEY_DESCRIPTION;
import static org.karunashraya.karunacare.utils.LConstants.KEY_MEDICATION;
import static org.karunashraya.karunacare.utils.LConstants.KEY_NAME;
import static org.karunashraya.karunacare.utils.LConstants.KEY_OPTIONS;
import static org.karunashraya.karunacare.utils.LConstants.KEY_SUGGESTION;
import static org.karunashraya.karunacare.utils.LConstants.VIEW_TYPE_DESCRIPTION;
import static org.karunashraya.karunacare.utils.LConstants.VIEW_TYPE_HEADER;

public class AdapterActions extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = AdapterActions.class.getSimpleName();

    private final LayoutInflater mLayoutInflater;

    private final ArrayList<ItemAction> actionItems;
    private final ArrayList<Integer>    selectionFlow;

    private final ActionCallbacks mActionCallback;

    private JSONObject mCurrentJson;
    private JSONObject currentJsonObject;

    private Handler handler;

    public AdapterActions(Context context, ActionCallbacks actionCallback, String fileName) {

        actionItems = new ArrayList<>();
        selectionFlow = new ArrayList<>();

        mActionCallback = actionCallback;

        mLayoutInflater = LayoutInflater.from(context);

        mCurrentJson = LApplication.getJsonObject(fileName);
        currentJsonObject = LApplication.getJsonObject(fileName);
    }

    public String getTitle() {
        return actionItems.get(0).title;
    }

    public void showActions() {
        try {
            String prevDecision = "";
            String name         = "";
            String description  = "";
            String medication   = "";
            String suggestion   = "";
            String decision     = "";

            name = mCurrentJson.getString(KEY_NAME);

            try {
                prevDecision = actionItems.get(actionItems.size() - 1).decision;
            } catch (Exception e) {
//                //e.printStackTrace();
            }

            if (mCurrentJson.has(KEY_DESCRIPTION)) {
                description = mCurrentJson.getString(KEY_DESCRIPTION);
                description = convertToHtml(description);
            }

            if (mCurrentJson.has(KEY_MEDICATION)) {
                medication = mCurrentJson.getString(KEY_MEDICATION);
            }

            if (mCurrentJson.has(KEY_SUGGESTION)) {
                suggestion = mCurrentJson.getString(KEY_SUGGESTION);
            }

            if (mCurrentJson.has(KEY_DECISION)) {
                decision = mCurrentJson.getString(KEY_DECISION);
            }

            if (actionItems.size() > 1 && VIEW_TYPE_DESCRIPTION == actionItems.get(actionItems.size() - 1).itemType) {
                actionItems.remove(actionItems.size() - 1);
                actionItems.get(actionItems.size() - 1).isCollapsed = true;
            }

            if (!description.equals("") || !medication.equals("") || !suggestion.equals("") || !decision.equals("")) {

                actionItems.add(new ItemAction(VIEW_TYPE_HEADER,
                        " " + prevDecision + " " + name,
                        description,
                        medication,
                        suggestion,
                        decision));

                actionItems.add(new ItemAction(VIEW_TYPE_DESCRIPTION,
                        "",
                        description,
                        medication,
                        suggestion,
                        decision));
            } else {
                actionItems.add(new ItemAction(VIEW_TYPE_HEADER,
                        " " + prevDecision + " " + name,
                        "",
                        medication,
                        suggestion,
                        decision));
            }

        } catch (JSONException e) {
            //e.printStackTrace();
        }

        notifyDataSetChanged();

        mActionCallback.onOptionsSet(getOptions());
    }

    private String convertToHtml(String description) {
        return "<body style='text-align:justify; ;  ' >" + description + "</body>";
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;

        switch (viewType) {
            case VIEW_TYPE_HEADER: {
                view = mLayoutInflater.inflate(R.layout.item_title, parent, false);
                return new HeaderHolder(view);
            }

            case VIEW_TYPE_DESCRIPTION: {
                view = mLayoutInflater.inflate(R.layout.item_description, parent, false);
                return new ViewHolder(view);
            }
        }

        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        ItemAction itemAction = actionItems.get(position);

        switch (holder.getItemViewType()) {
            case VIEW_TYPE_HEADER: {
                ((HeaderHolder) holder).tvTitle.setText(itemAction.title);

                if (itemAction.isCollapsed) {
                    ((HeaderHolder) holder).ivArrow.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
                } else {
                    ((HeaderHolder) holder).ivArrow.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
                }

                if (itemAction.description.equals("") &&
                        itemAction.medication.equals("") &&
                        itemAction.suggestion.equals("")) {
                    ((HeaderHolder) holder).ivArrow.setVisibility(View.INVISIBLE);
                } else {
                    ((HeaderHolder) holder).ivArrow.setVisibility(View.VISIBLE);
                }
            }
            break;

            case VIEW_TYPE_DESCRIPTION: {

                if (!itemAction.description.equals("")) {
//                    ((ViewHolder) holder).tvDescription.setVisibility(View.VISIBLE);

//                    if (Build.VERSION.SDK_INT >= 19) {
//                        ((ViewHolder) holder).tvDescription.setLayerType(View.LAYER_TYPE_HARDWARE, null);
//                    } else {
//                        ((ViewHolder) holder).tvDescription.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
//                    }

//                    ((ViewHolder) holder).tvDescription.setVisibility(View.VISIBLE);

//                    slideToRight(((ViewHolder) holder).tvDescription);

//                    Log.d(TAG, "onBindViewHolder: " + itemAction.description);

                    if (Build.VERSION.SDK_INT > 19) {
                        ((ViewHolder) holder).tvDescription.setAlpha(0.0f);
                        ((ViewHolder) holder).tvDescription.setVisibility(View.INVISIBLE);
                    }

//                    ((ViewHolder) holder).tvDescription.setWebViewClient(((ViewHolder) holder).mWebViewClient);
                    ((ViewHolder) holder).tvDescription.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);

                    ((ViewHolder) holder).tvDescription.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
                    if (Build.VERSION.SDK_INT >= 19) {
                        ((ViewHolder) holder).tvDescription.setLayerType(View.LAYER_TYPE_HARDWARE, null);
                    } else {
                        ((ViewHolder) holder).tvDescription.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    }

                    if (Build.VERSION.SDK_INT <= 19) {
                        showDelayed(((ViewHolder) holder).tvDescription, itemAction.description);
                    } else {
                        ((ViewHolder) holder).tvDescription.loadData(
                                itemAction.description,
                                "text/html; charset=utf-8",
                                null);
                    }

//                    ((ViewHolder) holder).tvDescription.loadDataWithBaseURL(
//                            null,
//                            itemAction.description,
//                            "text/html; charset=utf-8",
//                            "UTF-8",
//                            null);

                    if (Build.VERSION.SDK_INT > 19) {
                        ((ViewHolder) holder).tvDescription.animate()
                                .translationY(0)
                                .alpha(1.0f)
//                            .setDuration(50)
                                .setListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        super.onAnimationEnd(animation);
                                        ((ViewHolder) holder).tvDescription.setVisibility(View.VISIBLE);
                                    }
                                });
                    }
//                    ((ViewHolder) holder).tvDescription.setText(LUtils.fromHtml(itemAction.description));
                } else {
//                    ((ViewHolder) holder).tvDescription.animate().alpha(0.0f);

                    if (Build.VERSION.SDK_INT > 19) {
                        ((ViewHolder) holder).tvDescription.setAlpha(0.0f);
                        ((ViewHolder) holder).tvDescription.setVisibility(View.INVISIBLE);
                    }
                }

//                if (!itemAction.medication.equals("")) {
//                    ((ViewHolder) holder).tvMedication.setVisibility(View.VISIBLE);
//                    ((ViewHolder) holder).tvMedication.setText(itemAction.medication);
//                } else {
//                    ((ViewHolder) holder).tvMedication.setVisibility(View.GONE);
//                }
//
//                if (!itemAction.suggestion.equals("")) {
//                    ((ViewHolder) holder).tvSuggestion.setVisibility(View.VISIBLE);
//                    ((ViewHolder) holder).tvSuggestion.setText(itemAction.suggestion);
//                } else {
//                    ((ViewHolder) holder).tvSuggestion.setVisibility(View.GONE);
//                }
//
//                if (!itemAction.decision.equals("") && itemAction.showDecision) {
//                    ((ViewHolder) holder).tvDecision.setVisibility(View.VISIBLE);
//                    ((ViewHolder) holder).tvDecision.setText(itemAction.decision);
//                } else {
//                    ((ViewHolder) holder).tvDecision.setVisibility(View.GONE);
//                }
            }
            break;
        }
    }

    private void showDelayed(final WebView webView, final String description) {

        if (null == handler) {
            handler = new Handler();
        }

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                webView.loadData(
                        description,
                        "text/html; charset=utf-8",
                        null);
            }
        }, 500);
    }

    public void slideToRight(View view) {
        TranslateAnimation animate = new TranslateAnimation(0, view.getWidth(), 0, 0);
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.GONE);
    }

    public void slideToLeft(View view) {
        TranslateAnimation animate = new TranslateAnimation(0, view.getWidth(), 0, 0);
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return actionItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return actionItems.get(position).itemType;
    }

    public void optionSelected(int position) {

        try {
            selectionFlow.add(position);
            mCurrentJson = mCurrentJson.getJSONArray(KEY_OPTIONS).getJSONObject(position);

            showActions();

        } catch (JSONException e) {
            //e.printStackTrace();
        }
    }

    public String[] getOptions() {

        if (mCurrentJson.has(KEY_OPTIONS)) {

            try {

                JSONArray array = mCurrentJson.getJSONArray(KEY_OPTIONS);

                String options[] = new String[array.length()];

                for (int i = 0; i < array.length(); ++i) {
                    options[i] = array.getJSONObject(i).getString(KEY_NAME);
                }

                return options;

            } catch (JSONException e) {
                //e.printStackTrace();
            }
        }

        return null;
    }

    private void titleClicked(int adapterPosition) {

        int descriptionCount = 0;
        int selectionFlowPosition;

        int startPosition = adapterPosition + 1;

        try {
            if (VIEW_TYPE_DESCRIPTION == actionItems.get(startPosition).itemType) {
                ++startPosition;
            }

            // Remove action items
            int size = actionItems.size();

            if (size == startPosition) {
                return;
            }

            for (int i = startPosition; i < size; ++i) {
                actionItems.remove(startPosition);
            }

            actionItems.get(actionItems.size() - 1).showDecision = true;

        } catch (Exception e) {
            //e.printStackTrace();
        }

        // Find the position in selectionFlow
        for (int i = 0; i < adapterPosition; ++i) {
            if (VIEW_TYPE_DESCRIPTION == actionItems.get(i).itemType) {
                ++descriptionCount;
            }
        }

        selectionFlowPosition = adapterPosition - descriptionCount;

        // Remove the selectionFlow items
        int size = selectionFlow.size();

        for (int i = selectionFlowPosition; i < size; ++i) {
            selectionFlow.remove(selectionFlowPosition);
        }

        expandItems();

        notifyDataSetChanged();

        // Find the currentJson
        findCurrentJson();
    }


    private void expandItems() {
        try {
            // Find the last header, try to expand it
            int size = actionItems.size();

            ItemAction itemAction;

            if (VIEW_TYPE_HEADER == actionItems.get(size - 1).itemType) {
                itemAction = actionItems.get(size - 1);
            } else {
                itemAction = actionItems.get(size - 2);
            }

            if (!itemAction.description.equals("")
                    || !itemAction.medication.equals("")
                    || !itemAction.suggestion.equals("")
                    || !itemAction.decision.equals("")) {

                if (itemAction.isCollapsed) { // Expand

                    actionItems.add(size, new ItemAction(VIEW_TYPE_DESCRIPTION,
                            "",
                            itemAction.description,
                            itemAction.medication,
                            itemAction.suggestion,
                            itemAction.decision));

                    itemAction.isCollapsed = false;
                    notifyItemInserted(size + 1);
                }
            }
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    public boolean onBackPressed() {

        int size = actionItems.size();

        int headerCount = 0;

        for (int i = 0; i < size; ++i) {
            if (VIEW_TYPE_HEADER == actionItems.get(i).itemType) {
                ++headerCount;
            }
        }

        if (1 == headerCount) {
            return true;
        }

        // Remove the last item from selectionFlow
        if (VIEW_TYPE_DESCRIPTION == actionItems.get(size - 1).itemType) {
            actionItems.remove(size - 1);
            actionItems.remove(size - 2);
        } else {
            actionItems.remove(size - 1);
        }

        actionItems.get(actionItems.size() - 1).showDecision = true;

        selectionFlow.remove(selectionFlow.size() - 1);

        expandItems();

        notifyDataSetChanged();

        // Find the currentJson
        findCurrentJson();

        return false;
    }

    private void findCurrentJson() {

        mCurrentJson = currentJsonObject;

        for (Integer position : selectionFlow) {
            try {
                mCurrentJson = mCurrentJson.getJSONArray(KEY_OPTIONS).getJSONObject(position);
            } catch (JSONException e) {
                //e.printStackTrace();
            }
        }

        mActionCallback.onOptionsSet(getOptions());
    }

    private class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final WebView       tvDescription;
        //        private final TextView tvMedication;
//        private final TextView tvSuggestion;
//        private final TextView tvDecision;
        private       WebViewClient mWebViewClient;

        ViewHolder(View itemView) {
            super(itemView);

            tvDescription = (WebView) itemView.findViewById(R.id.tv_description);
//            tvMedication = (TextView) itemView.findViewById(R.id.tv_medication);
//            tvSuggestion = (TextView) itemView.findViewById(R.id.tv_suggestion);
//            tvDecision = (TextView) itemView.findViewById(R.id.tv_decision);

            mWebViewClient = new ContentWebViewClient();

            itemView.setOnClickListener(this);
        }

        private class ContentWebViewClient extends WebViewClient {
            private boolean mTimeout;

            public ContentWebViewClient() {
                mTimeout = true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {

//            mWebViewContent.setVisibility(View.GONE);
//                LLog.d(TAG, "wv: onPageStarted");
//            new Thread(new Runnable() {
//
//                @Override
//                public void run() {
//
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            mErrorLayout.setRefreshing(false);
//                            showLoading();
//                            hideError();
//                        }
//                    });
//
//                    try {
//                        Thread.sleep(LConstants.TIMEOUT_DURATION_DETAILS);
//                    } catch (InterruptedException e) {
//                        LLog.printStackTrace(e);
//                    }
//
//                    if (mTimeout) {
//
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                mErrorLayout.setRefreshing(false);
//                                hideLoading();
//                                mLoadingLatest.setVisibility(View.GONE);
//                                hideLatestNews();
//                                showError();
//                            }
//                        });
//                    }
//                }
//
//            }).start();
            }

            @Override
            public void onPageFinished(final WebView view, String url) {
//                LLog.d(TAG, "wv: onPageFinished");

                mTimeout = false;

//                slideToLeft(view);

//                view.animate()
//                        .translationY(0)
//                        .alpha(1.0f)
//                        .setDuration(50)
//                        .setListener(new AnimatorListenerAdapter() {
//                            @Override
//                            public void onAnimationEnd(Animator animation) {
//                                super.onAnimationEnd(animation);
//                                view.setVisibility(View.VISIBLE);
//                            }
//                        });
//                view.animate().alpha(1.0f);
//
//                view.animate()
//                        .alpha(1.0f)
//                        .setDuration(300)
//                        .setListener(new AnimatorListenerAdapter() {
//                            @Override
//                            public void onAnimationEnd(Animator animation) {
//                                super.onAnimationEnd(animation);
//                                view.setVisibility(View.VISIBLE);
//                            }
//                        });

//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    loadComments();
//                }
//            }, 200);
//
//            hideLoading();
//            hideError();
//            mWebViewContent.setVisibility(View.VISIBLE);
            }

        }

        @Override
        public void onClick(View view) {
            titleClicked(getAdapterPosition() - 1);
        }
    }

    private class HeaderHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final TextView  tvTitle;
        private final ImageView ivArrow;


        HeaderHolder(View itemView) {
            super(itemView);

            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            ivArrow = (ImageView) itemView.findViewById(R.id.iv_arrow);

            ivArrow.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();

            switch (view.getId()) {
                case R.id.iv_arrow: {

                    ItemAction itemAction = actionItems.get(position);

                    if (!itemAction.description.equals("")
                            || !itemAction.medication.equals("")
                            || !itemAction.suggestion.equals("")) {

                        if (itemAction.isCollapsed) { // Expand

                            actionItems.add(position + 1, new ItemAction(VIEW_TYPE_DESCRIPTION,
                                    "",
                                    itemAction.description,
                                    itemAction.medication,
                                    itemAction.suggestion,
                                    itemAction.decision));

                            if ((actionItems.size() - 2) != position) {
                                actionItems.get(position + 1).showDecision = false;
                            }

                            itemAction.isCollapsed = false;
                            notifyItemInserted(position + 1);

                            ivArrow.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);

                            mActionCallback.onItemExpanded(position + 1);

                        } else { // Collapse

                            itemAction.isCollapsed = true;
                            actionItems.remove(position + 1);

                            ivArrow.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);

                            notifyItemRemoved(position + 1);

                        }
                    }
                }
                break;

                default: {
                    titleClicked(getAdapterPosition());
                }
                break;
            }
        }
    }
}
