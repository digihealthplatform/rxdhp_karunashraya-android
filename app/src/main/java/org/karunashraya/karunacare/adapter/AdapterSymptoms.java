package org.karunashraya.karunacare.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.karunashraya.karunacare.activity.ActivityActions;
import org.karunashraya.karunacare.app.LApplication;
import org.karunashraya.karunacare.utils.LConstants;
import org.karunashrayacare.R;

import java.util.ArrayList;

public class AdapterSymptoms extends RecyclerView.Adapter<AdapterSymptoms.ViewHolder> {

    private static final String TAG = AdapterSymptoms.class.getSimpleName();

    private final Context mContext;

    private final ArrayList<String>  mSymptoms;
    private final ArrayList<String>  mJsonFiles;
    private final ArrayList<Integer> mImages;

    private JSONArray mJArray;

    public AdapterSymptoms(Context context, String fileName) {
        mContext = context;

        mSymptoms = new ArrayList<>();
        mJsonFiles = new ArrayList<>();
        mImages = new ArrayList<>();

        try {
            JSONObject jObject = LApplication.getJsonModel(fileName);

            mJArray = jObject.getJSONArray(LConstants.KEY_SYMPTOMS);

            for (int i = 0; i < mJArray.length(); ++i) {
                JSONObject jsonObject = mJArray.getJSONObject(i);

                mSymptoms.add(jsonObject.getString(LConstants.KEY_NAME));
                mJsonFiles.add(jsonObject.getString(LConstants.KEY_FILE_NAME));
            }

        } catch (JSONException e) {
            //e.printStackTrace();
        }

        mImages.add(R.drawable.nv);
        mImages.add(R.drawable.constipation);
        mImages.add(R.drawable.delirium);
        mImages.add(R.drawable.breathlessness);
        mImages.add(R.drawable.neuropathic_pain);
        mImages.add(R.drawable.pain);
        mImages.add(R.drawable.oral_care);
        mImages.add(R.drawable.skin_problems);
        mImages.add(R.drawable.miscellaneous);
    }

    @Override
    public AdapterSymptoms.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(mContext).inflate(R.layout.item_symptom, parent, false);
        view.getLayoutParams().height = (parent.getMeasuredHeight() / 3) - 35;

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterSymptoms.ViewHolder holder, int position) {
        holder.tvTitle.setText(mSymptoms.get(position));
        holder.imageView.setImageResource(mImages.get(position));
    }

    @Override
    public int getItemCount() {
        return mImages.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView  tvTitle;
        ImageView imageView;

        ViewHolder(View itemView) {
            super(itemView);

            tvTitle = (TextView) itemView.findViewById(R.id.title);
            imageView = (ImageView) itemView.findViewById(R.id.im_symptom);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(mContext, ActivityActions.class);
            intent.putExtra(LConstants.KEY_FILE_NAME, mJsonFiles.get(getAdapterPosition()));

            mContext.startActivity(intent);
        }
    }
}
