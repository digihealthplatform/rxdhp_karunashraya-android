package org.karunashraya.karunacare.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class AdapterSpinnerNoSelect<T> extends ArrayAdapter {

    public AdapterSpinnerNoSelect(Context context, int spinner_layout, List<T> list) {
        super(context, spinner_layout, list);
    }

//    @Override
//    public boolean isEnabled(int position) {
//        return position != 0;
//    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {

        View     view = super.getDropDownView(position, convertView, parent);
        TextView tv   = (TextView) view;

        tv.setTextColor(Color.BLACK);

        return view;
    }
}
