package org.karunashraya.karunacare.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.karunashraya.karunacare.interfaces.OptionCallbacks;
import org.karunashrayacare.R;

public class AdapterOptions extends RecyclerView.Adapter<AdapterOptions.ViewHolder> {

    private static final String TAG = AdapterOptions.class.getSimpleName();

    private final LayoutInflater mLayoutInflater;
    private       String         mOptions[];

    private final OptionCallbacks mOptionCallback;

    public AdapterOptions(Context context, OptionCallbacks optionCallback) {
        mOptionCallback = optionCallback;
        mOptions = new String[0];

        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_option, parent, false);
        return new AdapterOptions.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvOption.setText(mOptions[position]);
    }

    @Override
    public int getItemCount() {

        if (null == mOptions) {
            return 0;
        }

        return mOptions.length;
    }

    public void setOptions(String options[]) {
        mOptions = options;

        notifyDataSetChanged();
        mOptionCallback.onOptionsCount(getItemCount());
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvOption;

        ViewHolder(View itemView) {
            super(itemView);

            tvOption = (TextView) itemView.findViewById(R.id.tv_option);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mOptionCallback.onOptionSelected(getAdapterPosition());
        }
    }
}
