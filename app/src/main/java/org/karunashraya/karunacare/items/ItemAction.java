package org.karunashraya.karunacare.items;

public class ItemAction {

    public final int itemType;

    public boolean isCollapsed;
    public boolean showDecision;

    public final String medication;
    public final String suggestion;
    public final String decision;

    public final String title;
    public final String description;

    public ItemAction(int itemType, String title, String description, String medication, String suggestion, String decision) {
        this.itemType = itemType;
        this.title = title;
        this.description = description;
        this.medication = medication;
        this.suggestion = suggestion;
        this.decision = decision;
        this.showDecision = true;
    }
}
