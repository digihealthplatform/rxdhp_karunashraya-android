package org.karunashraya.karunacare.app;

import android.app.Application;
import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;
import org.karunashraya.karunacare.utils.LUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import static org.karunashraya.karunacare.utils.LUtils.generateKey;

public class LApplication extends Application {
    private static LApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
    }

    public static Context getAppContext() {
        return mInstance.getApplicationContext();
    }

    public static String readFromAssets(Context context, String fileName) throws IOException {
        SecretKey secret;
        String    result = "";

//        try {
//            secret = generateKey();
        InputStream inputStream = context.getAssets().open(fileName);
        byte[]      fileBytes   = new byte[inputStream.available()];
        inputStream.read(fileBytes);
        inputStream.close();
//            result = LUtils.decryptString(fileBytes, secret);

        result = new String(fileBytes);

//        } catch (NoSuchAlgorithmException
//                | NoSuchPaddingException
//                | InvalidParameterSpecException
//                | InvalidKeyException
//                | UnsupportedEncodingException
//                | IllegalBlockSizeException
//                | BadPaddingException
//                | InvalidAlgorithmParameterException
//                | InvalidKeySpecException e) {
//            //e.printStackTrace();
//        }

        return result;
    }

    public static String readFromFile(String fileName) {
        File file = new File(fileName);

        BufferedReader reader;

        StringBuilder sb = new StringBuilder();

        try {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
            String mLine = reader.readLine();

            while (mLine != null) {
                sb.append(mLine);
                mLine = reader.readLine();
            }

            reader.close();
        } catch (IOException e) {
            //e.printStackTrace();
        }

        return sb.toString();
    }

    public static JSONObject getJsonModel(String fileName) {
        try {
            String modelString = readFromAssets(getAppContext(), fileName);
            return new JSONObject(modelString);
        } catch (IOException | JSONException e) {
            //e.printStackTrace();
        }

        return null;
    }

    public static JSONObject getJsonObject(String fileName) {
        try {
            String modelString = readFromAssets(getAppContext(), fileName);
            return new JSONObject(modelString);
        } catch (IOException | JSONException e) {
            //e.printStackTrace();
        }

        return null;
    }
}
