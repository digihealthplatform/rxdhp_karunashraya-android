package org.karunashraya.karunacare.utils;

public class LConstants {

    public static final int VIEW_TYPE_HEADER      = 0;
    public static final int VIEW_TYPE_DESCRIPTION = 1;

    public static final String KEY_SYMPTOMS       = "symptoms";
    public static final String KEY_FILE_NAME      = "file_name";
    
    public static final String KEY_NAME           = "name";
    public static final String KEY_DESCRIPTION    = "description";
    public static final String KEY_MEDICATION     = "medication";
    public static final String KEY_SUGGESTION     = "suggestion";
    public static final String KEY_DECISION       = "decision";

    public static final String KEY_OPTIONS        = "options";
    public static final String KEY_SYMPTOM_INDEX  = "symptom_index";
}