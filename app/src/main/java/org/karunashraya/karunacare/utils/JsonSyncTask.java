package org.karunashraya.karunacare.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.google.gson.Gson;

import org.karunashraya.karunacare.app.LApplication;
import org.karunashraya.karunacare.asset.ItemUser;
import org.karunashrayacare.R;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import static org.karunashraya.karunacare.utils.LUtils.getCurrentDateTimeMillis;

public class JsonSyncTask extends AsyncTask<String, Integer, String> {

    private Gson gson;

    private File tempFolder;

    public static final String JSON_SYNC_URL = "http://13.126.85.194/cardio/pallikareuploadtoserver.php";

    public JsonSyncTask() {
        gson = new Gson();
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected void onPostExecute(String result) {
    }

    @Override
    protected String doInBackground(String... params) {

        tempFolder = new File(Environment.getExternalStorageDirectory(), LApplication.getAppContext().getResources().getString(R.string.temp_folder));

        if (!tempFolder.exists()) {
            tempFolder.mkdirs();
        }

        try {
            if (createAndUploadFile()) {
                LPref.putBooleanPref("data_synced", true);

                LPref.putStringPref("email", "");
                LPref.putStringPref("phone", "");
                LPref.putStringPref("specialization", "");
                LPref.putStringPref("sub_specialization", "");
                LPref.putStringPref("years_of_experience", "");
                LPref.putStringPref("years_of_experience_in_palliative_care", "");
                LPref.putStringPref("location", "");
                LPref.putStringPref("state", "");
                LPref.putStringPref("locationOther", "");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private boolean createAndUploadFile() throws Exception {
        ItemUser itemUser = new ItemUser();

        itemUser.setEmail(LPref.getStringPref("email", ""));
        itemUser.setPhone(LPref.getStringPref("phone", ""));
        itemUser.setSpecialization(LPref.getStringPref("specialization", ""));
        itemUser.setSubSpecialization(LPref.getStringPref("sub_specialization", ""));
        itemUser.setYearsOfExperience(LPref.getStringPref("years_of_experience", ""));
        itemUser.setYearsOfExperienceInPalliativeCare(LPref.getStringPref("years_of_experience_in_palliative_care", ""));
        itemUser.setLocation(LPref.getStringPref("location", ""));
        itemUser.setState(LPref.getStringPref("state", ""));
        itemUser.setLocationOther(LPref.getStringPref("locationOther", ""));

        String json = "{ \"userInfo\": " + gson.toJson(itemUser) + "}";

        String fileName = "karpal_" + itemUser.getPhone() + "_" + getCurrentDateTimeMillis() + ".json";

        final File outputFile = new File(tempFolder, fileName);

        FileOutputStream fos = new FileOutputStream(outputFile);
        fos.write(json.getBytes());
        fos.close();

        if (uploadFile(outputFile)) {
            outputFile.delete();
            return true;
        }

        outputFile.delete();
        return false;
    }

    private synchronized boolean uploadFile(File file) {
        int serverResponseCode = 0;

        HttpURLConnection connection;
        DataOutputStream  dataOutputStream;
        String            lineEnd    = "\r\n";
        String            twoHyphens = "--";
        String            boundary   = "*****";

        int    bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int    maxBufferSize = 1024 * 1024;

        try {
            FileInputStream fileInputStream = new FileInputStream(file);

            URL url = new URL(JSON_SYNC_URL);

            connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Connection", "Keep-Alive");
            connection.setRequestProperty("ENCTYPE", "multipart/form-data");
            connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            connection.setRequestProperty("uploaded_file", file.getName());

            dataOutputStream = new DataOutputStream(connection.getOutputStream());

            dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
            dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
                    + file.getName() + "\"" + lineEnd);

            dataOutputStream.writeBytes(lineEnd);

            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];

            bytesRead = fileInputStream.read(buffer, 0, bufferSize);

            while (bytesRead > 0) {
                dataOutputStream.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }

            dataOutputStream.writeBytes(lineEnd);
            dataOutputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            serverResponseCode = connection.getResponseCode();
            String serverResponseMessage = connection.getResponseMessage();

            fileInputStream.close();
            dataOutputStream.flush();
            dataOutputStream.close();

            return serverResponseCode == 200;

        } catch (FileNotFoundException e) {
            return false;

        } catch (MalformedURLException e) {
            return false;

        } catch (IOException e) {
            return false;
        }
    }
}