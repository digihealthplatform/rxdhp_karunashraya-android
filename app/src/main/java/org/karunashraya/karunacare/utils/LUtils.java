package org.karunashraya.karunacare.utils;

import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.text.Html;
import android.text.Spanned;
import android.widget.Spinner;

import org.karunashraya.karunacare.adapter.AdapterSpinner;
import org.karunashraya.karunacare.adapter.AdapterSpinnerNoSelect;
import org.karunashraya.karunacare.app.LApplication;
import org.karunashrayacare.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.security.spec.KeySpec;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class LUtils {

    public static byte[] encryptString(String message, SecretKey secret)
            throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidParameterSpecException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
        Cipher cipher;
        cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, secret);
        byte[] cipherText = cipher.doFinal(message.getBytes("UTF-8"));
        return cipherText;
    }

    public static String decryptString(byte[] cipherText, SecretKey secret)
            throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidParameterSpecException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, UnsupportedEncodingException {
        Cipher cipher;
        cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, secret);
        String decryptString = new String(cipher.doFinal(cipherText), "UTF-8");
        return decryptString;
    }

    public static SecretKey generateKey()
            throws NoSuchAlgorithmException, InvalidKeySpecException {

        // TODO: 1/9/2018, Change encryption method
        String password = "password";
        
        /* Store these things on disk used to derive key later: */
        int iterationCount = 1000;
        int saltLength     = 32; // bytes; should be the same size as the output (256 / 8 = 32)
        int keyLength      = 256; // 256-bits for AES-256, 128-bits for AES-128, etc

        /* When first creating the key, obtain a salt with this: */
//        SecureRandom random = new SecureRandom();
//        byte[] salt = new byte[saltLength];
//        random.nextBytes(salt);

        byte[] salt = {69, 121, 101, 45, 62, 118, 101, 114, 61, 101, 98};

       /* Use this to derive the key from the password: */
        KeySpec keySpec = new PBEKeySpec(password.toCharArray(), salt,
                iterationCount, keyLength);
        SecretKeyFactory keyFactory = SecretKeyFactory
                .getInstance("PBKDF2WithHmacSHA1");
        byte[] keyBytes = keyFactory.generateSecret(keySpec).getEncoded();

        return new SecretKeySpec(keyBytes, "AES");
    }

    public static void encryptAssets() {

        try {
            File dir = new File(Environment.getExternalStorageDirectory() + "/KarunashrayaCare/Json");

            if (dir.isDirectory()) {
                String[] children = dir.list();

                for (String aChildren : children) {
                    encryptFile(aChildren);
                }
            }
        } catch (IOException
                | NoSuchAlgorithmException
                | InvalidKeyException
                | NoSuchPaddingException
                | BadPaddingException
                | InvalidParameterSpecException
                | IllegalBlockSizeException
                | InvalidKeySpecException e) {
            //e.printStackTrace();
        }
    }

    private static void encryptFile(String fileName) throws IOException, InvalidKeySpecException, NoSuchAlgorithmException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, InvalidParameterSpecException, NoSuchPaddingException {

        SecretKey secret = generateKey();

        File outputPath = new File(Environment.getExternalStorageDirectory(), "KarunashrayaCare/Encrypted");

        byte[] encryptedBytes = LUtils.encryptString(LApplication.readFromFile(
                Environment.getExternalStorageDirectory() + "/KarunashrayaCare/Json/" + fileName), secret);

        File             outputFile = new File(outputPath.getPath(), fileName);
        FileOutputStream fos        = new FileOutputStream(outputFile);
        fos.write(encryptedBytes);
        fos.close();
    }

    public static String fromHtml(String source) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY).toString();
        } else {
            return Html.fromHtml(source).toString();
        }
    }

//    public static Spanned fromHtml(String source) {
//        return Html.fromHtml(source);
//    }

    public static void setSpinnerAdapter(Context context, Spinner spinner, String[] list) {
        final AdapterSpinner<String> adapterSpinner = new AdapterSpinner<>(
                context,
                R.layout.spinner_layout,
                new ArrayList<>(Arrays.asList(list))
        );
        adapterSpinner.setDropDownViewResource(R.layout.spinner_layout);
        spinner.setAdapter(adapterSpinner);
    }

    public static void setSpinnerAdapterNoSelect(Context context, Spinner spinner, String[] list) {
        final AdapterSpinnerNoSelect<String> adapterSpinner = new AdapterSpinnerNoSelect<>(
                context,
                R.layout.spinner_layout,
                new ArrayList<>(Arrays.asList(list))
        );
        adapterSpinner.setDropDownViewResource(R.layout.spinner_layout);
        spinner.setAdapter(adapterSpinner);
    }

    public static long getCurrentDateTimeMillis() {
        return Calendar.getInstance().getTimeInMillis();
    }
}
