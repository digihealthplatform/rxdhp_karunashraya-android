package org.karunashraya.karunacare.interfaces;

public interface ActionCallbacks {
    
    void onOptionsSet(String options[]);

    void onItemExpanded(int position);
}
