package org.karunashraya.karunacare.interfaces;

public interface OptionCallbacks {
    void onOptionSelected(int position);

    void onOptionsCount(int count);
}
