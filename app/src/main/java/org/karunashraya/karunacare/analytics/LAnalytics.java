package org.karunashraya.karunacare.analytics;

import android.content.Context;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.karunashrayacare.R;

public class LAnalytics {
    private GoogleAnalytics analytics;
    private Tracker         tracker;

    public LAnalytics(Context context) {
        analytics = GoogleAnalytics.getInstance(context);
        setDefaultTracker();
    }

    public void setScreenName(String screenName) {
        tracker.setScreenName(screenName);
        tracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    private synchronized void setDefaultTracker() {
        if (tracker == null) {
            tracker = analytics.newTracker(R.xml.global_tracker);
            tracker.enableAdvertisingIdCollection(true);
        }
    }
}