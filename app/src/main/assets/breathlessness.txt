{
  "name" : "Breathlessness",
  
  "description" : "
    <b><span style='color:teal'>Definition:</span></b>
    <br>
    Breathlessness (or dyspnoea) is a subjective experience of breathing discomfort that varies in intensity.

    <p>
    <b><span style='color:teal'>General Notes:</span></b>
    <p>Shortness of breath is common in advanced cancer.
    <p>It is also frequently seen in heart failure, Chronic Obstructive Pulmonary Disease (COPD) and other lung diseases, eg fibrosis and infections.
    <p>It is distressing for patients and their carers.",

  "options" : [
  {
    "name" : "Assessment",

    "description" : "Only the patient can judge
      <li>The severity, ie how bad / disabling </li>
      <li>The impact eg: fear and anxiety</li>
      <p>Standard tests (eg spirometry) do not indicate the effect of breathlessness and are of little value
      <p>Elicit patient’s concerns
      <p>What are the precipitating / alleviating factors?
      <p>Can any of the underlying causes be reversed?"
  },
  {
    "name" : "Causes",
    "description" : "Choose from below:",
    "options" : [
      {
        "name" : "Cancer related",
        "description" : "
        <ul>
            <li>Primary or secondary lung cancer (and associated airway obstruction / lymphangitis) </li>
            <li>Pleural / pericardial effusion </li>
            <li>Superior Vena Caval Obstruction (SVCO) </li>
            <li>Phrenic nerve palsy
            <li>Fatigue / weakness </li>
            <li>Ascites </li>
        </ul>"
      },
      {
        "name" : "Treatment related",
        "description" : "
        <ul>
           <li>Surgery (lobectomy / pneumonectomy)</li>
	       <li>Radiotherapy / chemotherapy (pneumonitis / fibrosis)</li>
	       <li>medications causing fluid retention or bronchospasm</li>
	       <li>tracheostomy: ?blocked by secretions</li>
        </ul>"
      },
      {
        "name" : "Other conditions",
        "description" : "
        <ul>
            <li>Infection</li>
            <li>Heart failure</li>
	        <li>Pulmonary embolus</li>
	        <li>Pneumothorax</li>
	        <li>COPD</li>
        </ul>"
      },
      {
        "name" : "Existing debilitating conditions",
        "description" : "
        <ul>
        <li>Infection</li>
	    <li>Anaemia</li>
	    <li>Fatigue / muscle weakness</li>
        </ul>"
      },
      {
        "name" : "Co-existing Psychological factors",
        "description" : "
        <ul>
          <li>Fear / anxiety</li>
	      <li>Distress</li>
	      <li>Claustrophobia</li>
          </ul>"
      }
    ]
  },
  {
    "name" : "Management",
    "description" : "Choose from below:",
    "options" : [
      {
        "name" : "General",
        "description" : "Many of the above are potentially reversible and should be treated where appropriate.
<p>Optimise current therapy (interventions /doses / frequency)
<p>Acknowledge patient’s fears
<p>NB if <u>Stridor</u> or <u>SVCO</u>, is it appropriate to refer to specialist?   (eg stenting or Radiotherapy.)
<p>If not consider High dose steroids:   (See under corticosteroids)"
      },
      {
        "name" : "Non-Pharmacological ",
        "description" : "
        <ul>
          <li>Fan</li>
	      <li>Facial cooling</li>
	      <li>Positioning to improve function / ease of breathing eg sitting upright</li>
	      <li>Relaxation techniques</li>
	      <li>Distraction / goal setting</li>
	      <li>Breathing exercises</li>
	      <li>Energy conservation</li>
	      <li>Accupuncture</li>
        </ul>"
      },
      {
        "name" : "Pharmacological",
        "description" : "The following are applicable at all stages, including the terminal phase",
        "options" : [
          {
            "name" : "First choice",
            "description" : "
            <span style='color:red'>Morphine</span>
            <br>
            Effective and safe. It reduces the excess ventilatory drive and response to hypoxia / hypercapnia.
            Reduces the sensation of breathlessness.
            <br>
            It does not cause CO2 retention in the doses given below:

              <p>
              ● Patient never taken opioids previously:
                <ul>
                    <li>Start  <b>2mg po</b>,  4 to 6 hourly    and 2mg as necessary  in between. </li>
                    <li>Increase by 50% daily till effective dose reached.</li>
                    <li>Doses in excess of 20mg 4 hourly are unlikely to be produce further benefit</li>
                </ul>

              <p>
              ● Patient already on morphine for pain:
              <ul>
                <li>Give additional 30 to 50% of current dose </li>
              </ul>

              <p>
              ● Unable to swallow:
              <ul>
                <li>Start with 1mg, <b>sc</b>, 4 to 6 hourly  and 1 mg sc prn in addition </li>
                <li>Increase by 50% daily till effective dose attained. </li>
              </ul>

               <p>
              ● Frail or elderly or renal impairment:
              <ul>
              <li>Halve the doses above and titrate upwards with caution. </li>
              </ul>"
          },
          {
            "name" : "Second choice",
            "description" : "

            <span style='color:red'>Corticosteroids</span>
            <br>

            Particularly useful in lymphangitis for reducing peri-tumour oedema
	          <ul>
                <li><span style='color:red'>Dexamethasone</span> 8 to 16mg daily (po or SC)</li>
	            <li>(alternative is : <span style='color:red'>Prednisolone</span> 60mg po daily)</li>
                <li>Give in the mornings or in divided doses before 4.00pm</li>
                <li>Review after one week and reduce to minimum effective dose</li>
                <li>If no response then STOP</li>
              </ul>
              <span style='color:teal'>NB if <u>Stridor</u> or <u>SVCO</u>, is it appropriate to refer to specialist? eg: Stenting or Radiotherapy</span>
              ",
            "decision" : "NB if Stridor or SVCO, is it appropriate to refer to specialist? eg: Stenting or Radiotherapy",
            "options" : [
              {
                "name" : "No",
                "description" : "
                <span style='color:red'>Dexamethasone</span> 16mg po / sc, stat and then daily.
                <br>
                <b>or</b> (<span style='color:red'>Prednisolone</span> 60mg po stat and daily).
                <br>
                Consider high dose steroids. Review and reduce as above
                "
              }
            ]
          },
          {
            "name" : "Third choice",
            "description" : "
            <span style='color:red'>Benzodiazepines</span>
            <p>
            Useful for associated panic / anxiety / sensation of suffocation.
            <p>
            Reduce fear and panic in hyperventilation
            <p>
            Can be added to the above (opioids and steroids)
            Use any of the following:
            <ul>
              <li><span style='color:red'>Lorazepam</span>  0.5 to 1mg, sub-lingual , 6 to 8 hourly, or as needed</li>
              <li><span style='color:red'>Diazepam</span> 2 to 5mg, two or three times daily or as needed (useful at night)</li>
              <li><span style='color:red'>Midazolam</span>  2 to 5 mg, SC, 4-6hourly if unable to swallow</li>
            </ul>

              <br>
              "
          },
          {
            "name" : "Other choices",
            "description" : "Choose from below:",
            "options" : [
              {
                "name" : "Inhaled therapy",
                "decision" : "If wheezy?",
                "description" : "<span style='color:teal'>If wheezy?</span>",
                "options" : [
                  {
                    "name" : "Yes",
                    "description" : "
                    <span style='color:red'>Salbutamol</span> 2.5 to 5 mg via an inhaler or nebuliser  x4 per day
                    <p>
                    (May also help even if no obvious wheeze, bronchospasm sometimes not associated with wheeze)
                    <p>
                    <span style='color:teal'>If still wheezy?</span>",
                    "decision" : "If still wheezy?",
                    "options" : [
                      {
                        "name" : "Yes",
                        "description" : "
                        Add <span style='color:red'>Ipratropium bromide</span> 250 to 500microgram nebules x4 per day
                        <p>
                        Alternatively consider <span style='color:red'>Theophylline</span> MR 200mg bd
                        <p>
                        NB normal saline inhalations may help expectoration of sputum / tenacious secretions"
                      }
                    ]
                  }
                ]
              },
              {
                "name" : "Oxygen",
                "decision" : "If Oxygen saturation (pulse oximeter) less than 90%?",
                "description" : "<span style='color:teal'>If Oxygen saturation (pulse oximeter) less than 90%?</span>",
                "options" : [
                  {
                    "name" : "Yes",
                    "description" : "Use the lowest level to achieve a saturation greater than 90%.
                    <p>
                    May also help in a few non-hypoxic patients. However, responses often poor.
                    <p>
                    In COPD patients, DO NOT exceed 4-6 l/min. Saturation can be maintained at 85%.
                    Nasal prongs may be better tolerated than a mask
                    <p>Try and avoid long-term / continuous use. 
                    <p>Costly and often of little benefit.
                    <p>NB mechanical ventilation not appropriate in advanced disease
                    "
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  }
  ]
}